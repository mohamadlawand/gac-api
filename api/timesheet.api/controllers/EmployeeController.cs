﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;
using timesheet.model.Dtos;
using timesheet.model.Factory;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        readonly EmployeeService employeeService;
        readonly EmployeeFactory _employeeFactory = new EmployeeFactory();

        public EmployeeController(EmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpGet("getall")]
        public async Task<IActionResult> GetAll()
        {
            IEnumerable<Employee> items = await this.employeeService.GetEmployees();

            IEnumerable<EmployeeDto> dtos = items.Select(x => _employeeFactory.CreateEmployeeDTO(x));

            return new ObjectResult(dtos);
        }
    }
}