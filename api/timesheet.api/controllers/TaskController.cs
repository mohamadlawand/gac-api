using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;
using timesheet.model.Dtos;

namespace timesheet.api.controllers
{
    [Route("api/v1/task")]
    [ApiController]
    public class TaskController
    {
        readonly TaskService _taskService;
        readonly timesheet.model.Factory.TaskFactory _taskFactory = new timesheet.model.Factory.TaskFactory();
        public TaskController(TaskService taskService, IMapper mapper)
        {
            this._taskService = taskService;
            //_mapper = mapper;
        }

        [HttpPost("AddTask")]
        public async Task<IActionResult> AddTask(TaskDto task)
        {
            model.Task _exist = await _taskService.CheckTask(task.Name);
            
           
            //System.Console.Write("Exist" + _exist + "\n");

            model.Task _task= new  model.Task() {
                Description = task.Description,
                Name = task.Name
            };
            // var taskToCreate = _mapper.Map<model.Task>(task);

            bool result = await _taskService.Add(_task);

            return new ObjectResult(result);
        }

        [HttpGet("getall")]
        public async Task<IActionResult> GetAll()
        {
            var items = await _taskService.GetAllTasks();

            IEnumerable<TaskDto> dtos = items.Select(x => _taskFactory.CreateTasktDTO(x));
            return new ObjectResult(dtos);
        }
    }
}