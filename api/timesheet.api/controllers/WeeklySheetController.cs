using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;
using timesheet.model.Dtos;
using timesheet.model.Factory;

namespace timesheet.api.controllers
{
    [Route("api/v1/weeklysheet")]
    [ApiController]
    public class WeeklySheetController : ControllerBase
    {
        readonly WeeklySheetService _weeklySheetService;
        readonly WeeklySheetFactory _sheetFactory = new WeeklySheetFactory();
        
        public WeeklySheetController(WeeklySheetService weeklySheetService, WeeklySheetFactory sheetFactory)
        {
            this._weeklySheetService = weeklySheetService;
        }

        [HttpGet("getall")]
        public async Task<IActionResult> GetAll()
        {
            IEnumerable<WeeklySheet> items = await _weeklySheetService.GetAllWeeklySheets();
            return new ObjectResult(items);
        }

        [HttpGet("getallperemployee/{id?}")]
        public async Task<IActionResult> GetAllPerEmployee(int id)
        {
            // Get the number of the sheets for the user 
            IEnumerable<WeeklySheet> items = await _weeklySheetService.GetAllWeeklySheetsForEmployee(id);
            System.Console.Write("Number " + items.Count() + "\n");

            // Convert the list to Dto so it can be sent as json
            IEnumerable<WeeklySheetDto> dto = items.Select(x => _sheetFactory.CreateWeeklysheetDTO(x));

            return new ObjectResult(dto);
        }

        [HttpGet("GetAllPerEmployeeWeek/{id?}")]
        public async Task<IActionResult> GetAllPerEmployeeWeek(int id, int weekId)
        {
            // Get the number of the sheets for the user 
            IEnumerable<WeeklySheet> items = await _weeklySheetService.GetAllWeeklySheetsForEmployeePerWeek(id, weekId);
            System.Console.Write("Number " + items.Count() + "\n");

            // Convert the list to Dto so it can be sent as json
            IEnumerable<WeeklySheetDto> dto = items.Select(x => _sheetFactory.CreateWeeklysheetDTO(x));

            return new ObjectResult(dto);
        }

        [HttpPost("AddWeeklysheetBatch")]
        public async Task<IActionResult> AddWeeklysheetBatch(ExampleData sheets)
        {
            
            System.Console.Write("Number " + sheets.sheets.Count() + "\n");
            // Check if task exist 

            // Add Task

            foreach(WeeklySheetDto sheet in sheets.sheets)
            {
                WeeklySheet _exist = await _weeklySheetService.GetAllWeeklySheetsForEmployeePerWeekPerTask(sheet.EmployeeId,sheet.WeekId, sheet.Task);

                WeeklySheet _sheet = new WeeklySheet() {
                    EmployeeId = sheet.EmployeeId,
                    Friday = sheet.Friday,
                    Monday = sheet.Monday,
                    Saturday = sheet.Saturday,
                    Tuesday = sheet.Tuesday,
                    Thursday = sheet.Thursday,
                    Sunday = sheet.Sunday,
                    WeekId = sheet.WeekId,
                    Wednesday = sheet.Wednesday,
                    TaskId = sheet.Task
                };

                System.Console.Write(_sheet == null ? " no " : " yes " + sheet.Task + "\n");

                if(_exist == null)
                    await _weeklySheetService.Add(_sheet);
                else
                    await _weeklySheetService.Edit(_sheet);
            }

            

            return new ObjectResult(true);
        }
    }

    public class ExampleData
    {
        public IList<WeeklySheetDto> sheets { get; set; }
    }
}