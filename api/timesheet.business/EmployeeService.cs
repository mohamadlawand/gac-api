﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public async Task<IEnumerable<Employee>> GetEmployees()
        {
            return await this.db.Employees.Include(x => x.WeeklySheets).ToListAsync();
        }
    }
}
