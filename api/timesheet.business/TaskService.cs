using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class TaskService
    {
        public TimesheetDb db { get; }
        public TaskService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public async Task<IEnumerable<model.Task>> GetAllTasks()
        {
            return await this.db.Tasks.ToListAsync();
        }

        public async Task<bool> Add(model.Task _task)
        {
            try
            {
                await this.db.Tasks.AddAsync(_task);
                await db.SaveChangesAsync();
                return true;
            } 
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public async Task<model.Task> CheckTask(string name)
        {
            return await this.db.Tasks
                                    .Where(x => x.Name == name)
                                    .FirstOrDefaultAsync();
        }
    }
}