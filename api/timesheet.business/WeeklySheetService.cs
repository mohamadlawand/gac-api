using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class WeeklySheetService
    {
        public TimesheetDb db { get; }
        public WeeklySheetService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        // Get Weekly sheets
        public async Task<IEnumerable<WeeklySheet>> GetAllWeeklySheets()
        {
            return await this.db.WeeklySheets.ToListAsync();
        }

        /// Get all weekly sheets for employee per employee If
        public async Task<IEnumerable<WeeklySheet>> GetAllWeeklySheetsForEmployee(int employeeId)
        {
            return await this.db.WeeklySheets
                                    .Where(x => x.EmployeeId == employeeId)
                                    .Include(x => x.Task)
                                    .ToListAsync();
        }

        /// Get all weekly sheets for employee per employee If
        public async Task<IEnumerable<WeeklySheet>> GetAllWeeklySheetsForEmployeePerWeek(int employeeId, int weekId)
        {
            return await this.db.WeeklySheets
                                    .Where(x => x.EmployeeId == employeeId
                                                    && x.WeekId == weekId)
                                    .ToListAsync();
        }

        public async Task<WeeklySheet> GetAllWeeklySheetsForEmployeePerWeekPerTask(int employeeId, int weekId, int taskId)
        {
            return await this.db.WeeklySheets
                                    .Where(x => x.EmployeeId == employeeId
                                                    && x.WeekId == weekId
                                                    && x.TaskId == taskId)
                                    .FirstOrDefaultAsync();
        }

        public async Task<bool> Add(WeeklySheet _sheet)
        {
            try
            {
                await this.db.WeeklySheets.AddAsync(_sheet);
                await db.SaveChangesAsync();
                return true;
            } 
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public async Task<bool> Edit(WeeklySheet _sheet)
        {
            WeeklySheet _exist = await this.db.WeeklySheets
                                            .Where(x => x.EmployeeId == _sheet.EmployeeId
                                                            && x.WeekId == _sheet.WeekId
                                                            && x.TaskId == _sheet.TaskId)
                                            .FirstOrDefaultAsync();

            if(_exist != null)
            {
                try
                {
                    _exist.Monday = _sheet.Monday;
                    _exist.Saturday = _sheet.Saturday;
                    _exist.Sunday = _sheet.Sunday;
                    _exist.TaskId = _sheet.TaskId;
                    _exist.Thursday = _sheet.Thursday;
                    _exist.Tuesday = _sheet.Tuesday;
                    _exist.Wednesday = _sheet.Wednesday;
                    _exist.WeekId = _sheet.WeekId;

                    await db.SaveChangesAsync();

                    return true;
                }
                catch(Exception ex)
                {
                    System.Console.Write("error " + ex.Message + "\n");
                }
            }

            return false;
        }
    }
}