﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace timesheet.data.Migrations
{
    public partial class WeeklySheetV7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WeeklySheet_Employee",
                table: "WeeklySheets");

            migrationBuilder.DropForeignKey(
                name: "FK_WeeklySheet_Task",
                table: "WeeklySheets");

            migrationBuilder.AddForeignKey(
                name: "FK_WeeklySheets_Employees_EmployeeId",
                table: "WeeklySheets",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WeeklySheets_Tasks_TaskId",
                table: "WeeklySheets",
                column: "TaskId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WeeklySheets_Employees_EmployeeId",
                table: "WeeklySheets");

            migrationBuilder.DropForeignKey(
                name: "FK_WeeklySheets_Tasks_TaskId",
                table: "WeeklySheets");

            migrationBuilder.AddForeignKey(
                name: "FK_WeeklySheet_Employee",
                table: "WeeklySheets",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WeeklySheet_Task",
                table: "WeeklySheets",
                column: "TaskId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
