﻿using Microsoft.EntityFrameworkCore;
using System;
using timesheet.model;

namespace timesheet.data
{
    public class TimesheetDb : DbContext
    {
        public TimesheetDb(DbContextOptions<TimesheetDb> options)
            : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<WeeklySheet> WeeklySheets { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // modelBuilder.Entity<WeeklySheet>(entity => 
            // {
            //     entity.HasOne(e => e.Employee)
            //             .WithMany(p => p.WeeklySheets)
            //                 .HasForeignKey(d => d.EmployeeId)
            //                     .OnDelete(DeleteBehavior.Restrict)
            //                         .HasConstraintName("FK_WeeklySheet_Employee");

            //     entity.HasOne(t => t.Task)
            //             .WithMany(p => p.WeeklySheets)
            //                 .HasForeignKey(d => d.TaskId)
            //                     .OnDelete(DeleteBehavior.Restrict)
            //                         .HasConstraintName("FK_WeeklySheet_Task");
            // });
        }
    }
}
