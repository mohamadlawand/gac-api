namespace timesheet.model.Dtos
{
    public class EmployeeDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int TotalWeeklyEffort { get; set; }
        public double AverageWeeklyEffort { get; set; }
    }
}