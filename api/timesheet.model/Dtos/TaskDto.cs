using System.ComponentModel.DataAnnotations;

namespace timesheet.model.Dtos
{
    public class TaskDto
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public string Description { get; set; }
    }
}