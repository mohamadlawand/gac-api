using System;
using System.ComponentModel.DataAnnotations;

namespace timesheet.model.Dtos
{
    public class WeeklySheetDto
    {
        [Required]
        public int Sunday { get; set; }

        [Required]
        public int Monday { get; set; }

        [Required]
        public int Tuesday { get; set; }

        [Required]
        public int Wednesday { get; set; }

        [Required]
        public int Thursday { get; set; }

        [Required]
        public int Friday { get; set; }

        [Required]
        public int Saturday { get; set; }

        [Required]
        public int Task { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public int WeekId { get; set; }
        
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }

        public WeeklySheetDto()
        {
            Created = DateTime.Now;
            LastActive = DateTime.Now;
        }
    }
}