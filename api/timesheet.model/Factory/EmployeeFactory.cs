using System;
using System.Collections.Generic;
using timesheet.model.Dtos;

namespace timesheet.model.Factory
{
    public class EmployeeFactory
    {
        public EmployeeDto CreateEmployeeDTO(Employee _employee)
        {
            IEnumerable<WeeklySheet> _sheets = _employee.WeeklySheets;

            int _total = 0;

            try
            {
                foreach(WeeklySheet _sheet in _sheets)
                {
                    _total += _sheet.Sunday;
                    _total += _sheet.Monday;
                    _total += _sheet.Tuesday;
                    _total += _sheet.Wednesday;
                    _total += _sheet.Thursday;
                    _total += _sheet.Friday;
                    _total += _sheet.Saturday;
                }
            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
            }

            return new EmployeeDto()
            {
                Id = _employee.Id,
                Name = _employee.Name,
                Code = _employee.Code,
                TotalWeeklyEffort = _total,
                AverageWeeklyEffort = _total / 52
            };
        }
    }
}