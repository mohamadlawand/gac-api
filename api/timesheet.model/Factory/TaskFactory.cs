using timesheet.model.Dtos;

namespace timesheet.model.Factory
{
    public class TaskFactory
    {
        public TaskDto CreateTasktDTO(Task _task)
        {
            return new TaskDto()
            {
                Id = _task.Id,
                Name = _task.Name,
                Description = _task.Description
            };
        }
    }
}