using timesheet.model.Dtos;

namespace timesheet.model.Factory
{
    public class WeeklySheetFactory
    {
        public WeeklySheetDto CreateWeeklysheetDTO(WeeklySheet _sheet)
        {
            return new WeeklySheetDto()
            {
                EmployeeId = _sheet.EmployeeId,
                Friday = _sheet.Friday,
                Monday = _sheet.Monday,
                Tuesday = _sheet.Tuesday,
                Wednesday = _sheet.Wednesday,
                Thursday = _sheet.Thursday,
                Saturday = _sheet.Saturday,
                Sunday = _sheet.Sunday,
                Task = _sheet.TaskId,
                WeekId = _sheet.WeekId,
            };
        }
    }
}