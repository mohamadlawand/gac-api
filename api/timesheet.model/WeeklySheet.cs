using System.Collections.Generic;

namespace timesheet.model
{
    public class WeeklySheet
    {

        public int Id { get; set; }
        public int Sunday { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }
        public int TaskId { get; set; }
        public int EmployeeId { get; set; }
        public int WeekId { get; set; }

        public virtual Task Task {get;set;}
        public virtual Employee Employee { get; set; }
    }
}